package test;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {
	public static BunteRechteckeController bRController = new BunteRechteckeController();

	// x y breite hoehe
	public static void main(String[] args) {
		Rechteck rechteck0 = new Rechteck();
		rechteck0.setX(10).setY(10).setBreite(30).setHoehe(40);

		Rechteck rechteck1 = new Rechteck();
		rechteck1.setX(25).setY(25).setBreite(100).setHoehe(20);

		Rechteck rechteck2 = new Rechteck();
		rechteck2.setX(260).setY(10).setBreite(200).setHoehe(100);

		Rechteck rechteck3 = new Rechteck();
		rechteck3.setX(5).setY(500).setBreite(300).setHoehe(25);

		Rechteck rechteck4 = new Rechteck();
		rechteck4.setX(100).setY(100).setBreite(100).setHoehe(100);

		Rechteck rechteck5 = new Rechteck(200, 200, 200, 200);
		Rechteck rechteck6 = new Rechteck(800, 400, 20, 20);
		Rechteck rechteck7 = new Rechteck(800, 450, 20, 20);
		Rechteck rechteck8 = new Rechteck(850, 400, 20, 20);
		Rechteck rechteck9 = new Rechteck(855, 455, 25, 25);
		System.out.println(rechteck0.toString());
		

		bRController.add(rechteck0);
		bRController.add(rechteck1);
		bRController.add(rechteck2);
		bRController.add(rechteck3);
		bRController.add(rechteck4);
		bRController.add(rechteck5);
		bRController.add(rechteck6);
		bRController.add(rechteck7);
		bRController.add(rechteck8);
		bRController.add(rechteck9);
		System.out.println(bRController.toString());
		

		Rechteck eck10 = new Rechteck(-4, -5, -50, -200);
		System.out.println(eck10);
		Rechteck eck11 = new Rechteck();
		eck11.setX(-10);
		eck11.setY(-10);
		eck11.setBreite(-200);
		eck11.setHoehe(-100);
		System.out.println(eck11);
		System.out.println(rechteckeTesten());
	}
	public static boolean rechteckeTesten() {
		// return true wenn alle drin sind
		Rechteck ganzes_feld = new Rechteck(0,1000,1200,1000);
		Rechteck[] rechtecke_arr = new Rechteck[50000];
		for(int i = 0; i < rechtecke_arr.length; i++) {
			rechtecke_arr[i] = ganzes_feld.generiereZufallsRechteck();
			if(!ganzes_feld.enthaelt(rechtecke_arr[i])) {
				return false;
			}
		}
		return true;
	}
}
