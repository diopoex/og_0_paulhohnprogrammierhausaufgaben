package model;

import java.util.Random;
import java.awt.Rectangle;

public class Rechteck {
	private Punkt punkt;
	private int breite;
	private int hoehe;

	public Rechteck() {
		this.punkt = new Punkt(0, 0);
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		this.punkt = new Punkt(x, y);
		this.setBreite(breite);
		this.setHoehe(hoehe);
	}

	public int getX() {
		return this.punkt.getX();
	}

	public Rechteck setX(int x) {
		// return this; damit ich mit dem punktoperator weiter aufrufen kann
		// z.b. setX().setY();
		this.punkt.setX(x);
		return this;
	}

	public int getY() {
		return this.punkt.getY();
	}

	public Rechteck setY(int y) {
		this.punkt.setY(y);
		return this;
	}

	public int getBreite() {
		return breite;
	}

	public Rechteck setBreite(int breite) {
		this.breite = Math.abs(breite);
		return this;
	}

	public int getHoehe() {
		return hoehe;
	}

	public Rechteck setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
		return this;
	}

	@Override
	public String toString() {
		return "Rechteck [punkt=" + punkt + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}

	public boolean enthaelt(int x, int y) {
		if ((x >= this.punkt.getX() && x <= (this.punkt.getX() + this.breite))
				&& (y >= this.punkt.getY() - this.hoehe && y <= this.punkt.getY())) {
			return true;
		}
		return false;
	}

	public boolean enthaelt(Punkt p) {
		return this.enthaelt(p.getX(), p.getY());
	}

	public boolean enthaelt(Rechteck rechteck) {
		return this.enthaelt(rechteck.getX(), rechteck.getY())
				&& this.enthaelt(rechteck.getX() + rechteck.getBreite(), rechteck.getY() - rechteck.getHoehe());
		// x,y beschreibt den oberen linken punkt
		// ich prüfe ob dieser im anderen rechteck ist
		// und ob der untere rechte punkt im rechteck ist
		// wenn diese beiden punkte im rechteck sind ist das ganze rechteck im
		// rechteck
	}

	public static Rechteck generiereZufallsRechteck() {
		final int max_breite = 500;
		final int max_laenge = 500;
		Random rand = new Random();
		Rechteck ganzes_feld = new Rechteck(0, 1000, 1200, 1000);

		Rechteck r1 = new Rechteck(rand.nextInt(1201), rand.nextInt(1001), rand.nextInt(max_breite),
				rand.nextInt(max_laenge));
		while (!ganzes_feld.enthaelt(r1)) {
			r1 = new Rechteck(rand.nextInt(1201), rand.nextInt(1001), rand.nextInt(max_breite),
					rand.nextInt(max_laenge));
		}
		return r1;
	}

}
