package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;
import test.RechteckTest;

import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class eingabemaske extends JFrame {

	private JPanel contentPane;
	private JTextField textField_0;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	public BunteRechteckeController bRC = RechteckTest.bRController;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					eingabemaske frame = new eingabemaske();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public eingabemaske() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JLabel lblNewLabel = new JLabel("x:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);

		textField_0 = new JTextField();
		textField_0.setText("0");
		textField_0.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char charact = e.getKeyChar();
				if (((charact < '0') || (charact > '9')) && (charact != KeyEvent.VK_BACK_SPACE))
					e.consume();
			}
		});
		textField_0.setToolTipText("x");
		GridBagConstraints gbc_textField_0 = new GridBagConstraints();
		gbc_textField_0.insets = new Insets(0, 0, 5, 5);
		gbc_textField_0.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_0.gridx = 1;
		gbc_textField_0.gridy = 0;
		contentPane.add(textField_0, gbc_textField_0);
		textField_0.setColumns(10);

		JLabel lblY = new JLabel("y:");
		GridBagConstraints gbc_lblY = new GridBagConstraints();
		gbc_lblY.insets = new Insets(0, 0, 5, 5);
		gbc_lblY.anchor = GridBagConstraints.EAST;
		gbc_lblY.gridx = 0;
		gbc_lblY.gridy = 1;
		contentPane.add(lblY, gbc_lblY);

		textField_1 = new JTextField();
		textField_1.setText("0");
		textField_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char charact = e.getKeyChar();
				if (((charact < '0') || (charact > '9')) && (charact != KeyEvent.VK_BACK_SPACE))
					e.consume();
			}
		});
		textField_1.setToolTipText("y");
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 1;
		contentPane.add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);

		JLabel lblBreite = new JLabel("breite:");
		GridBagConstraints gbc_lblBreite = new GridBagConstraints();
		gbc_lblBreite.insets = new Insets(0, 0, 5, 5);
		gbc_lblBreite.anchor = GridBagConstraints.EAST;
		gbc_lblBreite.gridx = 0;
		gbc_lblBreite.gridy = 2;
		contentPane.add(lblBreite, gbc_lblBreite);

		textField_2 = new JTextField();
		textField_2.setText("0");
		textField_2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char charact = e.getKeyChar();
				if (((charact < '0') || (charact > '9')) && (charact != KeyEvent.VK_BACK_SPACE))
					e.consume();
			}
		});
		textField_2.setToolTipText("breite");
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 1;
		gbc_textField_2.gridy = 2;
		contentPane.add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);

		JLabel lblHhe = new JLabel("höhe");
		GridBagConstraints gbc_lblHhe = new GridBagConstraints();
		gbc_lblHhe.insets = new Insets(0, 0, 5, 5);
		gbc_lblHhe.anchor = GridBagConstraints.EAST;
		gbc_lblHhe.gridx = 0;
		gbc_lblHhe.gridy = 3;
		contentPane.add(lblHhe, gbc_lblHhe);

		textField_3 = new JTextField();
		textField_3.setText("0");
		textField_3.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char charact = e.getKeyChar();
				if (((charact < '0') || (charact > '9')) && (charact != KeyEvent.VK_BACK_SPACE))
					e.consume();
			}
		});
		textField_3.setToolTipText("höhe");
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.insets = new Insets(0, 0, 5, 5);
		gbc_textField_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_3.gridx = 1;
		gbc_textField_3.gridy = 3;
		contentPane.add(textField_3, gbc_textField_3);
		textField_3.setColumns(10);

		JButton btnNewButton_1 = new JButton("Öffne RechteckViewTest");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// man kann halt diesen bRC hier noch übergeben, in
				// RechteckViewTest nen bisschen was verändern und dann werden
				// die hier eingetragenen dinger angezeigt
				test.RechteckViewTest.main(null);
			}
		});
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_1.gridx = 4;
		gbc_btnNewButton_1.gridy = 3;
		contentPane.add(btnNewButton_1, gbc_btnNewButton_1);

		JButton btnNewButton_2 = new JButton("Rechteck Eintragen");
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//
				bRC.add(new Rechteck(Integer.parseInt(textField_0.getText()), Integer.parseInt(textField_1.getText()),
						Integer.parseInt(textField_2.getText()), Integer.parseInt(textField_3.getText())));
			}
		});
		GridBagConstraints gbc_btnNewButton_2 = new GridBagConstraints();
		gbc_btnNewButton_2.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton_2.gridx = 1;
		gbc_btnNewButton_2.gridy = 4;
		contentPane.add(btnNewButton_2, gbc_btnNewButton_2);

		JButton btnNewButton = new JButton("Beenden");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
		});
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 4;
		gbc_btnNewButton.gridy = 4;
		contentPane.add(btnNewButton, gbc_btnNewButton);
	}

}
