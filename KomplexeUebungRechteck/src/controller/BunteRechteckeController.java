package controller;

import java.util.LinkedList;
import java.util.List;

import model.Rechteck;

public class BunteRechteckeController {
	public static void main(String[] args) {

	}

	private List<Rechteck> listeRechtecke;

	public BunteRechteckeController() {
		this.listeRechtecke = new LinkedList<Rechteck>();
	}

	public void add(Rechteck r) {
		listeRechtecke.add(r);
	}

	public void reset() {
		listeRechtecke.clear();
	}

	public List<Rechteck> get() {
		return this.listeRechtecke;
	}

	@Override
	public String toString() {
		String rechteckeStr = "";
		for (int i = 0; i < listeRechtecke.size() - 1; i++) {
			rechteckeStr += listeRechtecke.get(i) + ", ";
		}
		rechteckeStr += listeRechtecke.get(listeRechtecke.size() - 1);
		String rStr = String.format("BunteRechteckeController [rechtecke=[%s]]", rechteckeStr);
		
		return rStr;
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		reset();
		Rechteck ganzes_feld = new Rechteck(0,1000,1200,1000);
		for(int i= 0; i<anzahl; i++) {
			add(ganzes_feld.generiereZufallsRechteck());
		}
	}
}
