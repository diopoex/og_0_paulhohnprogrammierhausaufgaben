package test;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.BunteRechteckeController;
import model.Rechteck;
import view.Zeichenflaeche;

public class RechteckViewTest extends JPanel {

	public static void main(String[] args) {
		
		RechteckTest.main(new String[0]);
		BunteRechteckeController bRC=RechteckTest.bRController;
		bRC.generiereZufallsRechtecke(25);
		
		System.out.println(bRC.get().size());
		JFrame fenster = new JFrame("Rechtecke");
		fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Zeichenflaeche kleinesFenster = new Zeichenflaeche(bRC);
		fenster.add(kleinesFenster);
		fenster.setSize(700,700);
		fenster.setVisible(true);
	}
}
