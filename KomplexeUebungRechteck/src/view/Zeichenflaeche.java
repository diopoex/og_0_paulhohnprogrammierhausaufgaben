package view;

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;

import controller.BunteRechteckeController;
import model.Rechteck;

public class Zeichenflaeche extends JPanel {
	
	private final BunteRechteckeController BRC;
	
	public Zeichenflaeche(BunteRechteckeController bRC) {
		this.BRC=bRC;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		for (Rechteck r :BRC.get()) {
			g.setColor(Color.BLACK);
			g.drawRect(r.getX(), r.getY(), r.getBreite(), r.getHoehe());
			//g.fillRect(r.getX(), r.getY(), r.getBreite(), r.getHoehe());
		}
	}
}
